# Notice

This material contains various scripts to be used with [Abyss Pipeline Tool](https://gitlab.ifremer.fr/abyss-project/abyss-pipeline) to process data from:

ftp://ftp.ifremer.fr/ifremer/dataref/bioinfo/merlin/abyss/BioinformaticPipelineComparisons/

# Reference

in PCI Ecology 

Miriam I. Brandt, Blandine Trouche, Laure Quintric, Patrick Wincker, Julie Poulain, Sophie Arnaud-Haond (2019). A flexible pipeline combining bioinformatic correction tools for prokaryotic and eukaryotic metabarcoding. bioRxiv. https://doi.org/10.1101/717355
