---
title: "Data_refining bioinfo tests"
author: "Blandine Trouche adapted from Miriam Brandt"
date: "04 july 2019"
output: word_document
editor_options: 
  chunk_output_type: console
---
This script takes a MOTU table produced by DADA2 and produces a refined version  of that MOTU table:
by adding a simplified taxonomy category
by quality-filtering the data based on NC
by removing unassigned MOTUs 
(min MOTU relative abundance filtering not done as we apply LULU)
by removing non-target taxa based on primer choice

During the production of the refined MOTU table, the script makes various quality-check graphs.

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

##Data preparation
```{r upload packages, echo=FALSE}
library(readr)
library(here)
here()
dr_here()
getwd()

library(phyloseq);packageVersion("phyloseq")
library(decontam); packageVersion("decontam")
library(vegan);packageVersion("vegan")
library(ggplot2); packageVersion("ggplot2")
library(cowplot)
library(gridExtra)
library(forcats)
library(data.table)
library(magrittr)
library(hexbin)
library(seqinr)
library(splitstackshape)
library(reshape2)
```


Upload OTUs tables from controls, tsv \t or csv ; 
Outside of data.table: files with OTU IDs in first column, otherwise it may NOT work.
```{r upload data, include=FALSE}
counts<-fread(here("1_raw_MOTU_tables/dada2/10_ASVs_counts.tsv"))
taxo<-fread(here("1_raw_MOTU_tables/dada2/ASVs_rdp_blast_taxonomy.tsv"))
samples<-grep("ClusterID", colnames(counts), value = T, invert = T)
```

Make simplified taxonomy column in order to facilitate filtering and plotting
```{r Make simplified taxonomy, include=FALSE}
#create a new factor of simplified BLAST taxonomy:
taxo[grepl("Archaea", taxo$HitTaxonomy), Blast_taxo_BA:="Archaea"]
taxo[grepl("Bacteria", taxo$HitTaxonomy), Blast_taxo_BA:="Bacteria"]
taxo[is.na(Blast_taxo_BA), "Blast_taxo_BA"] <- "Unassigned"

taxo$HitTaxonomy <- gsub("^Root;", "", taxo$HitTaxonomy)

#create new columns of taxonomy that will be removed after new_taxonomy is up
taxo <- cbind(taxo, colsplit(taxo$HitTaxonomy, "\\;", names = c("Domain", "Phylum", "Class", "Order", "Family", "Genus", "Species")))
dim(taxo)

#creation of new_taxonomy that will be filled with interesting taxonomic taxo
taxo$BLAST_Taxon <- taxo$Phylum
taxo[grepl("Archaea;Multi-affiliation", taxo$HitTaxonomy), BLAST_Taxon:="Unknown Archaea"]
taxo[grepl("Bacteria;Multi-affiliation", taxo$HitTaxonomy), BLAST_Taxon:="Unknown Bacteria"]
taxo[grepl("Proteobacteria;Alphaproteobacteria", taxo$HitTaxonomy), BLAST_Taxon:="Alphaproteobacteria"]
taxo[grepl("Proteobacteria;Betaproteobacteria", taxo$HitTaxonomy), BLAST_Taxon:="Betaproteobacteria"]
taxo[grepl("Proteobacteria;Gammaproteobacteria", taxo$HitTaxonomy), BLAST_Taxon:="Gammaproteobacteria"]
taxo[grepl("Proteobacteria;Deltaproteobacteria", taxo$HitTaxonomy), BLAST_Taxon:="Deltaproteobacteria"]
taxo[grepl("Proteobacteria;Zetaproteobacteria", taxo$HitTaxonomy), BLAST_Taxon:="Zetaproteobacteria"]
taxo[grepl("Proteobacteria;Magnetococcia", taxo$HitTaxonomy), BLAST_Taxon:="Magnetococcia"]
taxo[grepl("Proteobacteria;Multi-affiliation", taxo$HitTaxonomy), BLAST_Taxon:="Unassigned Proteobacteria"]
sum(is.na(taxo$BLAST_Taxon)==TRUE)

levels(as.factor(taxo$BLAST_Taxon))
taxo$Domain = NULL
taxo$Phylum = NULL
taxo$Class = NULL
taxo$Order = NULL
taxo$Family = NULL
taxo$Genus = NULL
taxo$Species = NULL
dim(taxo)

# Define unassigned category
taxo[HitTaxonomy=="None", BLAST_Taxon:="Unassigned"]
taxo[is.na(HitTaxonomy) , BLAST_Taxon:="Unassigned"]

### RDP
#create a new factor of simplified RDP taxonomy
taxo[,RDP_Taxon:=tax.Phylum]
taxo[,RDP_taxo_BA:=tax.Kingdom]

# Define unassigned, unknown, there is no multi-affiliation with RDP
taxo[is.na(RDP_Taxon), RDP_Taxon:="Unassigned"]
# more resolution in proteobacteria
taxo[tax.Class=="Alphaproteobacteria", RDP_Taxon:="Alphaproteobacteria"]
taxo[tax.Class=="Betaproteobacteria", RDP_Taxon:="Betaproteobacteria"]
taxo[tax.Class=="Gammaproteobacteria", RDP_Taxon:="Gammaproteobacteria"]
taxo[tax.Class=="Deltaproteobacteria", RDP_Taxon:="Deltaproteobacteria"]
taxo[tax.Class=="Zetaproteobacteria", RDP_Taxon:="Zetaproteobacteria"]
taxo[tax.Class=="Magnetococcia", RDP_Taxon:="Magnetococcia"]
taxo[tax.Phylum=="Proteobacteria" & is.na(tax.Class), RDP_Taxon:="Unassigned Proteobacteria"]

levels(as.factor(taxo$RDP_Taxon))
```

define color-blind colour palettes
```{r define color-blind colour palettes, include=FALSE}
scale_55 = c("grey", "royalblue", "chartreuse3",  "red", "darkorange","cyan2", "darkgreen", "deepskyblue", "mediumorchid3","#89C5DA", "#DA5724", "#74D944", "#C84248", "#673770", "#D3D93E", "#38333E", "#508578", "#D7C1B1", "#689030",   "#AD6F3B", "#CD9BCD", "#D14285", "#6DDE88", "#652926", "#7FDCC0", "#8569D5", "#5E738F", "#D1A33D", "#8A7C64", "#599861", "blue4", "yellow1", "violetred", "#990000", "#99CC00", "#003300", "#00CCCC", "#9966CC", "#993366", "#990033", "#4863A0", "#000033", "#330000", "#00CC99", "#00FF33", "#00CCFF", "#FF9933", "#660066", "#FF0066", "#330000", "#CCCCFF", "#3399FF", "#66FFFF", "#B5EAAA","#FFE87C")
```

overview of the 15 most abundant taxa in raw data:
```{r overview raw data, fig.cap="Read and OTU abundance in raw data", echo=FALSE, fig.width=10, fig.height=6}
#Create a table with nb of OTUs and reads for each Taxon level
raw_observation_sum=rowSums(counts[,2:ncol(counts)])
raw_observation_sum=cbind(ClusterID=counts$ClusterID,raw_observation_sum)
taxo=merge(taxo, raw_observation_sum, by="ClusterID")
taxo$raw_observation_sum = as.numeric(taxo$raw_observation_sum)
raw.taxo = data.frame(n.OTU = table(taxo$BLAST_Taxon),
                      n.reads = xtabs(taxo$raw_observation_sum~taxo$BLAST_Taxon))

write.table(raw.taxo, file=here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","Taxonomic_composition_raw_data_BLAST.csv"), sep = ";", col.names = T, row.names = F)

raw.taxo.rdp = data.frame(n.OTU = table(taxo$RDP_Taxon),
                      n.reads = xtabs(taxo$raw_observation_sum~taxo$RDP_Taxon))
write.table(raw.taxo.rdp, file= here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","Taxonomic_composition_raw_data_RDP.csv"), sep = ";", col.names = T, row.names = F)

# plot overview raw data BLAST:
#keep the most abundant clades for plotting purposes
keep = names(sort(xtabs(taxo$raw_observation_sum~taxo$BLAST_Taxon),decreasing=T)[1:15])
keep; length(keep)
#Create new factor
raw.taxo$Main_blast_tax = as.factor(raw.taxo$n.OTU.Var1)
#Change that are not in keep to "Others"
levels(raw.taxo$Main_blast_tax)[!(levels(raw.taxo$Main_blast_tax) %in% keep)] = "Others"
# order following main_taxo:
raw.taxo$Main_blast_tax = factor(raw.taxo$Main_blast_tax)

otus.raw.data = ggplot(data=raw.taxo, aes(x=factor(1), fill= Main_blast_tax)) + 
  geom_bar(aes(y = n.OTU.Freq/sum(n.OTU.Freq)),stat = "identity") +
  theme_minimal()+
  scale_fill_manual(values=scale_55, name="BLAST Taxon")+
  ggtitle("ASV abundance (%)") +
  theme_void() + 
  coord_polar(theta = "y")+
  theme(legend.text = element_text(size=16),
        legend.title = element_text(size = 18),
        title = element_text(size = 18))

reads.raw.data = ggplot(data=raw.taxo, aes(x=factor(1), fill=Main_blast_tax)) + 
  geom_bar(aes(y = n.reads.Freq/sum(n.reads.Freq)),stat = "identity") +
  theme_minimal() +
  scale_fill_manual(values=scale_55, name= "BLAST Taxon")+
  ggtitle("Read abundance (%)") + 
  theme_void() + 
  coord_polar(theta = "y")+
  theme(legend.text = element_text(size=16),
        legend.title = element_text(size = 18),
        title = element_text(size = 18))

plot.raw=grid.arrange(otus.raw.data+theme(legend.position = "none"),reads.raw.data+theme(legend.position = "none"), nrow=1)
legend.raw=get_legend(otus.raw.data)
#add legend on panel
plot.raw.data=plot_grid(plot.raw,legend.raw, rel_widths = c(1.5, 0.5))
plot.raw.data
png(here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","1_Plot_Overview_raw_data_BLAST.png"),width=10,height=6,units='in',res=600)
plot.raw.data
dev.off()
pdf(here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","1_Plot_Overview_raw_data_BLAST.pdf"),width =10,height =6, bg="transparent")
plot.raw.data
dev.off()

# plot overview raw data RDP:
#keep the most abundant clades for plotting purposes
keep = names(sort(xtabs(taxo$raw_observation_sum~taxo$RDP_Taxon),decreasing=T)[1:15])
keep; length(keep)
#Create new factor
raw.taxo.rdp$Main_RDP_tax = as.factor(raw.taxo.rdp$n.OTU.Var1)
#Change that are not in keep to "Others"
levels(raw.taxo.rdp$Main_RDP_tax)[!(levels(raw.taxo.rdp$Main_RDP_tax) %in% keep)] = "Others"

otus.raw.data = ggplot(data=raw.taxo.rdp, aes(x=factor(1), fill=Main_RDP_tax)) + 
  geom_bar(aes(y = n.OTU.Freq/sum(n.OTU.Freq)),stat = "identity") +
  theme_minimal()+
  scale_fill_manual(values=scale_55, name="RDP Taxon")+
  ggtitle("ASV abundance (%)") +
  theme_void() + 
  coord_polar(theta = "y")+
  theme(legend.text = element_text(size=16),
        legend.title = element_text(size = 18),
        title = element_text(size = 18))

reads.raw.data = ggplot(data=raw.taxo.rdp, aes(x=factor(1), fill=Main_RDP_tax)) + 
  geom_bar(aes(y = n.reads.Freq/sum(n.reads.Freq)),stat = "identity") +
  theme_minimal() +
  scale_fill_manual(values=scale_55, name= "RDP Taxon")+
  ggtitle("Read abundance (%)") + 
  theme_void() + 
  coord_polar(theta = "y")+
  theme(legend.text = element_text(size=16),
        legend.title = element_text(size = 18),
        title = element_text(size = 18))

plot.raw=grid.arrange(otus.raw.data+theme(legend.position = "none"),reads.raw.data+theme(legend.position = "none"), nrow=1)
legend.raw=get_legend(otus.raw.data)
#add legend on panel
plot.raw.data=plot_grid(plot.raw,legend.raw, rel_widths = c(1.5, 0.5))
plot.raw.data
png(here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","1_Plot_Overview_raw_data_RDP.png"),width=10,height=6,units='in',res=600)
plot.raw.data
dev.off()
pdf(here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","1_Plot_Overview_raw_data_RDP.pdf"),width =10,height =6, bg="transparent")
plot.raw.data
dev.off()
```

## Import to phyloseq and refine (filter) the data
```{r make phyloseq object from raw MOTU table, include=FALSE}
#create otu matrix (selecting only sample columns)

#otumat=data[,grep("^Com\\d|^MARMINE|^AbyssTest3|^MDW",colnames(data)),with=F]
otumat=counts
otumat$ClusterID=NULL
otumat=as.matrix(otumat)
rownames(otumat) <- counts$ClusterID

#create taxonomy table
taxmat=as.matrix(taxo)
rownames(taxmat)=taxo$ClusterID

###create sample info metadata

#Get read and OTU abundance per sample with vegan: transpose to get samples in rows, OTUs in columns:
colnames(otumat)
otumat2=t(otumat)
# sum up presence/absence of OTUs in each row/sample (vegan package):
Nb_OTU=specnumber(otumat2)
Nb_Reads=rowSums(otumat2)
Nb_OTU_Reads=cbind.data.frame(Nb_OTU, Nb_Reads)
sample=rownames(Nb_OTU_Reads)
Nb_OTU_Reads=cbind.data.frame(sample, Nb_OTU_Reads)
nsple=nrow(Nb_OTU_Reads)
# add new Site,Dive, COre, and Processing variables manually, length of variables adjusted to the number of samples
sple <- as.data.table(Nb_OTU_Reads)
sple <- sple[order(sample)]
sple$Site <- vector(mode = "character", length = nsple)
#sple$Sequencing <- vector(mode = "character", length = nsple)
sple$Core <- vector(mode = "character", length = nsple)
#sple$Dive <- vector(mode = "character", length = nsple)
sple$Pipeline <- vector(mode = "character", length = nsple)
sple$Pipeline=c("Dada2")
#sple$Processing <- vector(mode = "character", length = nsple)
sple$Sample_or_Control <- vector(mode = "character", length = nsple)
#fill new data variables
sple[grep("ESN",sample), Site:="ESN"]
sple[grep("CHR",sample), Site:="CHR"]
sple[grep("JAM",sample), Site:="JAM"]
sple[grep("PCT",sample), Site:="PCT-FA"]
sple[grep("ST117",sample), Site:="MDW-ST117"]
sple[grep("ST179",sample), Site:="MDW-ST179"]
sple[grep("ST201",sample), Site:="MDW-ST201"]
sple[grep("ST215",sample), Site:="MDW-ST215"]
sple[grep("ST22",sample), Site:="MDW-ST22"]
sple[grep("ST23",sample), Site:="MDW-ST23"]
sple[grep("ST38",sample), Site:="MDW-ST38"]
sple[grep("ST68",sample), Site:="MDW-ST68"]
sple[grep("MRM_ST35",sample), Site:="MRM-ST35"]
sple[grep("MRM_ST38",sample), Site:="MRM-ST38"]
sple[grep("MRM_ST48",sample), Site:="MRM-ST48"]

# core
sple[grep("CT1",sample), Core:="CT1"]
sple[grep("CT2",sample), Core:="CT2"]
sple[grep("CT3",sample), Core:="CT3"]

sple[grep("PL06",sample), Core:="CT1"]
sple[grep("PL07",sample), Core:="CT2"]
sple[grep("PL11",sample), Core:="CT3"]

# sple[grep("Blue_Core",sample), Core:="CT1"]
# sple[grep("Red_Core_1",sample), Core:="CT2"]
# sple[grep("Red_Core_2",sample), Core:="CT3"]

sple[grep("PC10",sample), Core:="CT1"]
sple[grep("PC12",sample), Core:="CT2"]
sple[grep("PC16",sample), Core:="CT3"]
sple[grep("PC04",sample), Core:="CT1"]
sple[grep("PC07",sample), Core:="CT2"]
sple[grep("PC08",sample), Core:="CT3"]
sple[grep("PC09",sample), Core:="CT1"]
sple[grep("PC15",sample), Core:="CT2"]

#controls
sple[grep("Mock|Negatif|control",sample), Site:="Control"]
#sple[grep("Mock|Negatif|control",sample), Dive:="Control"]
sple[grep("Mock|Negatif|control",sample), Core:="Control"]
sple[grep("Negatif|control",sample), Sample_or_Control:="Control Sample"]
sple[grep("Mock",sample), Sample_or_Control:="Mock Sample"]
sple[!grep("Mock|Negatif|control",sample), Sample_or_Control:="True Sample"]


#sequencing
#sple[grep("AbyssTest3b",sample), Sequencing:="Genseq_Miseq"]
#sple[grep("SLC",sample), Sequencing:="Genoscope_SLC_Hiseq"]
#sple[grep("NUII",sample), Sequencing:="Genoscope_NUII_Hiseq"]

# name sequencing duplicates
# sple$Sequencing <- sapply(sple$sample,function(X){
#   if(grepl("NUII$",X)){
#     "NUII"
#   }else{
#     "SLC"
#   }
# })

# export
fwrite(sple,here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","Sample_info.tsv"),sep="\t")
#fwrite(sple,"Sample_info.csv",sep=";")
#upload new sample info sheet containing read and OTU nbs but also Core nb, Site and Processign information:
sample_info_raw = read.table(here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","Sample_info.tsv"),header=TRUE,sep="\t")
dim(sample_info_raw)
rownames(sample_info_raw)
rownames(sample_info_raw)= sample_info_raw$sample

#construct the phyloseq object:
library(phyloseq);packageVersion("phyloseq")
library(ggplot2); packageVersion("ggplot2")
theme_set(theme_bw())
# define otu and taxonomy tables for phyloseq
OTU= otu_table(otumat, taxa_are_rows = TRUE)
TAX= tax_table(taxmat)
# make phyloseq object
ps.raw = phyloseq(OTU, sample_data(sample_info_raw), TAX)
```

remove contamination based on negative controls:
```{r remove contamination}
#library(decontam); packageVersion("decontam")
ps.raw
# Put sample_data into a ggplot-friendly data.frame
df <- as.data.frame(sample_data(ps.raw)) 
df$LibrarySize <- sample_sums(ps.raw)
df <- df[order(df$LibrarySize),]
df$Index <- seq(nrow(df))
# plot library size of NC and true samples
ggplot(data=df, aes(x=Index, y=LibrarySize, color=Sample_or_Control)) + geom_point()
png(here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","2_Library_sizes_samplesVScontrols.png"),width=8,height=6,units='in',res=600)
ggplot(data=df, aes(x=Index, y=LibrarySize, color=Sample_or_Control)) + geom_point()
dev.off()
pdf(here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","2_Library_sizes_samplesVScontrols.pdf"),width =8,height =6, bg="transparent")
ggplot(data=df, aes(x=Index, y=LibrarySize, color=Sample_or_Control)) + geom_point()
dev.off()
#detect contaminants, i.e. OTUs that are more abundant in controls than in samples
sample_data(ps.raw)$is.neg <- sample_data(ps.raw)$Sample_or_Control == "Control Sample"
contamdf.prev05 <- isContaminant(ps.raw, method="prevalence", neg="is.neg",threshold=0.5)
table(contamdf.prev05$contaminant)
ps.noncontam <- prune_taxa(!contamdf.prev05$contaminant, ps.raw)
ps.noncontam
#remove control samples
ps.raw.clean=subset_samples(ps.noncontam, Sample_or_Control != "Control Sample")
ps.raw.clean
#remove taxa that are not present in any of the remaining samples
ps.raw.clean= prune_taxa(taxa_sums(ps.raw.clean) > 0 ,ps.raw.clean)
ps.raw.clean
```

remove unassigned mOTUs:
```{r remove unassigned mOTUs}
# remove unassigned MOTUs
#ps.raw.pruned=subset_taxa(ps.raw.pruned, RDP_Taxon != "Unassigned")
#ps.raw.pruned
ps.raw.pruned=subset_taxa(ps.raw.clean, BLAST_Taxon != "Unassigned")
ps.raw.pruned

# OR remove unassigned MOTUs with read abundance <10
# ps.unassigned=subset_taxa(ps.raw.pruned, phylum == "Unassigned")
# ps.unassigned=filter_taxa(ps.unassigned, function(x) sum(x)<10,prune = TRUE)
# ps.raw.pruned.noNA=subset_taxa(ps.raw.pruned, phylum != "Unassigned")
# ps.raw.pruned=merge_phyloseq(ps.raw.pruned.noNA, ps.unassigned)
```

remove non-target taxa:
```{r remove non-target taxa}
ps.raw.pruned = prune_taxa((grepl("Eukaryota", ps.raw.pruned@tax_table[,15]) == FALSE), ps.raw.pruned)
ps.raw.pruned = prune_taxa((grepl("Chloroplast", ps.raw.pruned@tax_table[,15]) == FALSE), ps.raw.pruned)
ps.raw.pruned = prune_taxa((grepl("Mitochondria", ps.raw.pruned@tax_table[,15]) == FALSE), ps.raw.pruned)
ps.target <- ps.raw.pruned
ps.target
```

remove samples with insufficient sequencing depth
```{r remove samples with insufficient sequencing depth}
# inspect min reads of target taxa per sample before removal:
# reads per sample:
sort(sample_sums(ps.target), decreasing = FALSE)
# remove sample with low read number, less than 10,000 target taxa reads
ps.target.pruned = prune_samples(sample_sums(ps.target) > 10000, ps.target)
ps.target.pruned
#remove taxa that are not present in any of the remaining samples
ps.target.pruned= prune_taxa(taxa_sums(ps.target.pruned) > 0 ,ps.target.pruned)
ps.target.pruned
```

Quality-filtering on relative abundance commented:
```{r quality-filter MOTUs based on relative abundance per sample, include=FALSE}
# filter across MOTUs:remove all MOTUs that do not have a relative abundance > 0.01% (0.0001) in at least one sample
# e.g if all samples have 100,000 reads, a MOTU has to have at least once 10 reads

#HERE: relative abundance filter on target taxa: 11,912 taxa initially decreased to 2,221:
# ps.target.pruned.prop<-transform_sample_counts(ps.target.pruned, function(x) x/sum(x))
# relabunfilter<-filter_taxa(ps.target.pruned.prop, function(x) sum(x>0.0001)>1, prune = FALSE)
# ps.target.pruned.relabun <- prune_taxa(relabunfilter,ps.target.pruned)
# ps.target.pruned.relabun

#OR:
#library("genefilter")
#relabunfilter <- filterfun(kOverA(1, 0.0001))
#ps.raw.pruned <- prune_taxa(relabunfilter,ps.raw.clean)
```

## export refined data
```{r recap and export refined data}
#mean reads per sample
mean(sample_sums(ps.target.pruned))
#total number of MOTUs:
ntaxa(ps.target.pruned)
#total number of samples
nsamples(ps.target.pruned)
# define refined motu table, 
otumat.refined=as(otu_table(ps.target.pruned),"matrix")
otumat.refined=as.data.frame(otumat.refined)
ClusterID=rownames(otumat.refined)
otumat.refined=cbind(ClusterID, otumat.refined)
# define refined taxonomy table, 
taxmat.refined=as(tax_table(ps.target.pruned),"matrix")
#define refined sample info file
sample_info_refined=data.frame(sample_data(ps.target.pruned))
# define refined fasta file
x=data.frame(ClusterID)
# upload raw dada2 output fasta files with seqinr
fastas=read.fasta(file=here("1_raw_MOTU_tables/dada2/10_ASVs.fasta"), seqtype = "DNA")
fastas.refined=fastas[names(fastas) %in% x$ClusterID]

# number of MOTUs per sample
MOTU.abundance.per.sample= sample_sums(transform_sample_counts(ps.target.pruned, function(x) ifelse(x > 0, 1, x)))
#write.table(MOTU.abundance.per.sample, file= "MOTU_abundance_per_sample.csv", sep = ";", col.names = T, row.names = T)
sample_info_refined$Nb_ASVs_refined=MOTU.abundance.per.sample
sample_info_refined$Nb_reads_refined=sample_sums(ps.target.pruned)

# export
write.table(otumat.refined, file= here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","ASVs_counts_refined.csv"), sep = ";", col.names = T, row.names = F)
write.table(taxmat.refined, file= here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","TAX_table_refined.csv"), sep = ";", col.names = T, row.names = F)
write.table(sample_info_refined, file= here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","sample_info_refined.csv"), sep = ";", col.names = T, row.names = F)
write.table(otumat.refined, file= here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","ASVs_counts_refined.tsv"), sep = "\t", col.names = T, row.names = F)
write.table(taxmat.refined, file= here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST", "TAX_table_refined.tsv"), sep = "\t", col.names = T, row.names = F)
write.fasta(fastas.refined, ClusterID, file.out = here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","ASVs_refined.fasta"), open = "w")
```

## read track table per method
```{r read track table per method, include=FALSE}
# upload dada2 output readtrack table files
read.track1=fread(here("1_raw_MOTU_tables/dada2/8_readtrackfinal_H3WJNBCX2.csv"))
read.track2=fread(here("1_raw_MOTU_tables/dada2/8_readtrackfinal_HK3MKBCX2.csv"))
read.track1$sample=sub("_SLC","",read.track1$sample)
read.track=rbind(read.track1, read.track2)

# merge read.track with sample info
read.track=merge(read.track, sample_info_raw, by="sample")
#read.track.complete=merge(read.track, data.frame(MOTU.abundance.per.sample), by="sample")

# sum up rows of read track by method, to make overview of dada2 output
read.track.final=aggregate(. ~ Sample_or_Control, read.track, sum)

read.track.final$non_chimeric_seq_dada2=read.track.final$non_chimeric_reads
read.track.final$non_chimeric_reads=NULL
read.track.final$final_perc_reads_retained=NULL

#read.track.final$non_chimeric_seq_dada2_d1=read.track.final$Nb_Reads
read.track.final$Nb_Reads=NULL
read.track.final$perc_reads_retained=round((read.track.final$non_chimeric_seq_dada2/read.track.final$reads.in)*100,2)
read.track.final$perc_chimeras=round((100-(read.track.final$non_chimeric_seq_dada2/read.track.final$reads_before_chimera_removal)*100),2)

read.track.final$Number_of_initial_samples=read.track.final$Pipeline
read.track.final$Pipeline=NULL
read.track.final$Nb_OTU=NULL # as OTUs can be shared among samples so counted more than once...
read.track.final$Number_raw_clusters=ntaxa(ps.raw)
# add number of refined ASVs and reads:
read.track.final$Number_of_refined_samples=nlevels(sample_info_refined$sample)
read.track.final$Number_of_refined_reads=sum(sample_info_refined$Nb_reads_refined)
read.track.final$Number_refined_clusters=ntaxa(ps.target.pruned)

read.track.final$sample=NULL
read.track.final$Site=NULL
read.track.final$Core=NULL

#export
write.table(read.track.final, file= here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","read_track_per_method_blast.csv"), sep = ";", col.names = T, row.names = F)
```

## quality check after filtering
```{r quality check after filtering, fig.cap="Overall sequencing depth", echo=FALSE, fig.width=7, fig.height=4}
# Sequencing depth using frequency distribution of reads:

# boxplot of reads
depth.raw = ggplot(data=sample_info_refined, aes(x=Site,y=Nb_Reads)) + 
  geom_boxplot()+
  theme_bw() + 
  facet_wrap(~Pipeline)+
  labs(y="Number of raw reads", x="Site")+
  theme(text = element_text(size=14),
        axis.text.x = element_text(angle = 45, hjust=1,vjust=1))
depth.raw
png(here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","3_sequencing_depth.png"),width=7,height=4,units='in',res=400)
depth.raw
dev.off()
pdf(here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","3_sequencing_depth.pdf"),width =7,height =4, bg="transparent")
depth.raw
dev.off()

# boxplot of OTUs
depth.raw = ggplot(data=sample_info_refined, aes(x=Site,y=Nb_OTU)) + 
  geom_boxplot()+
  theme_bw() + 
  facet_wrap(~Pipeline)+
  labs(y="Number of raw ASVs", x="Site")+
  theme(text = element_text(size=14),
        axis.text.x = element_text(angle = 45, hjust=1,vjust=1))
depth.raw
png(here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","3_sequencing_depth_OTUs.png"),width=7,height=4,units='in',res=400)
depth.raw
dev.off()
pdf(here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","3_sequencing_depth_OTUs.pdf"),width =7,height =4, bg="transparent")
depth.raw
dev.off()
```

##Evaluation of sequencing depth with rarefaction curves
```{r rarefaction curves, fig.cap="Rarefaction curves",fig.width =10,fig.height =8, echo=FALSE}

# rarefy each sample by random subsampling in steps of 500, starting from 10 to the maximum number of reads in each sample
#WARNING requested 'sample' larger than smallest site maximum: happens when teh random subsample is greater than the actual number of reads in teh sample. This is why I limit to true sequencing depth after.
otumat.refined$ClusterID=NULL
rarC.raw = do.call("rbind", lapply(seq(10, max(sample_info_refined$Nb_reads_refined), 500), function(x) {
  #rarefy at a given depth (x)
  res = rarefy(t(otumat.refined), x)
  #format the results
  out = data.frame(Species=res[1:length(res)], Reads=rep(x, length(res)), sample=names(res), depth=sample_info_refined$Nb_reads_refined,Site=sample_info_refined$Site, Core= sample_info_refined$Core, Pipeline=sample_info_refined$Pipeline)
  out$Species[out$Reads>out$depth] = NA #limit to true sequencing depth
  out[which(is.na(out$Species)==F),]
}))

#plot, , color=Site
grarC.raw = ggplot(data= rarC.raw, aes(x=Reads, y=Species, color=Site))+
  geom_line(aes(group=sample), size =0.75) +
  theme_bw() + 
  facet_wrap(~Pipeline)+
  scale_color_manual(values=scale_55)+
  labs(y="Number  of ASVs", x="Number of Reads") +
  theme(axis.title=element_text(size=20),
        axis.text.x =  element_text(size=16, colour="black"),
        axis.text.y =  element_text(size=18, colour="black"),
        legend.text = element_text(size=18),
        legend.title = element_text(size=20),
        strip.text.x = element_text(size = 18))
grarC.raw
png(here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","4_rarefaction_curves.png"),width=10,height=8,units='in',res=600)
grarC.raw
dev.off()
pdf(here("2_refined_MOTU_tables/1_Data_refining_R/dada2/filtered_by_BLAST","4_rarefaction_curves.pdf"),width =10,height =8, bg="transparent")
grarC.raw
dev.off()
```

```{r}
rm(list=ls())
```

