# Notice

Directories contain shell script files that are configuration files fot the Abyss Pipeline Tool.

*BEFORE* using these scripts and tools, you have to declare the following variables:
- DATAREF: has to target the directory containing reference data
- DATAWORK: has to target a working directory where new files are created during pipeline execution

In all cases, use absolute path.

DATAREF should target the directory containg a copy of this content:

ftp://ftp.ifremer.fr/ifremer/dataref/bioinfo/merlin/abyss/BioinformaticPipelineComparisons/

For instance, if you have uploaded that content to this directory on your system:

`/foo/bar/BioinformaticPipelineComparisons`

then, do this:

`export DATAREF=/foo/bar/BioinformaticPipelineComparisons`

